package com.example.task02;

import java.io.IOException;

public class Task02Main {
    public static void main(String[] args) throws IOException {
        // чтобы протестировать свое решение, вам нужно:
        // - направить файл input.test в стандартный ввод программы (в настройках запуска программы в IDE или в консоли)
        // - направить стандартный вывод программы в файл output.test
        // - запустить программу
        // - и сравнить получившийся файл output.test с expected.test
        int firstByte = System.in.read();
        int secondByte;

        while ((secondByte = System.in.read()) != -1) {
            if (!(firstByte == 13 && secondByte == 10)) {
                System.out.write(firstByte);
            }
            firstByte = secondByte;
        }

        if (firstByte != -1) {
            System.out.write(firstByte);
        }

        System.out.flush();
    }

}

