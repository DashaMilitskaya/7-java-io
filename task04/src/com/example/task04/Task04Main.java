package com.example.task04;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

public class Task04Main {
    public static void main(String[] args) throws IOException {
        // чтобы протестировать свое решение, вам нужно:
        // - направить файл input.test в стандартный ввод программы (в настройках запуска программы в IDE или в консоли)
        // - запустить программу
        // - проверить, что получилось 351.731900


        String inStr = readAsString(System.in);
        String[] strArr = inStr.split("\\r+\\n|\\n| ");

        double sum = 0;

        for (String string : strArr) {
            try {
                sum += Double.parseDouble(string);
            } catch (NumberFormatException ignored) {

            }
        }

        System.out.println(String.format("%.6f",sum).replace(",","."));

    }
    public static String readAsString(InputStream inputStream) throws IOException {
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        int size;
        byte[] symBytes = new byte[4];

        if (inputStream == null) {
            throw new IllegalArgumentException();
        }

        while ((size = inputStream.read(symBytes, 0, symBytes.length)) != -1) {
            buf.write(symBytes, 0, size);
        }

        return buf.toString();

    }
}
