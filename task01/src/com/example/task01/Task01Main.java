package com.example.task01;

import org.hamcrest.core.IsNull;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Task01Main {
    public static void main(String[] args) throws IOException {
        //здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:


        System.out.println(checkSumOfStream(new ByteArrayInputStream(new byte[]{0x33, 0x45, 0x01})));


    }

    public static int checkSumOfStream(InputStream inStream) throws IOException {
        int chkSum = 0;
        int currentByte;
        
        if (inStream==null) {
            throw new IllegalArgumentException();
        }

        while ((currentByte = inStream.read()) != -1) {
            chkSum = Integer.rotateLeft(chkSum, 1) ^ currentByte;
        }

        return chkSum;
        
    }
}
