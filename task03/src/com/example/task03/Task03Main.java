package com.example.task03;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

public class Task03Main {
    public static void main(String[] args) throws IOException {
        //здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:

        /*
        System.out.println(readAsString(new FileInputStream("task03/src/com/example/task03/input.test"), Charset.forName("KOI8-R")));
        */
    }

    public static String readAsString(InputStream inputStream, Charset charset) throws IOException {
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        int size;
        byte[] symBytes = new byte[4];

        if (inputStream == null) {
            throw new IllegalArgumentException();
        }

        while ((size = inputStream.read(symBytes, 0, symBytes.length)) != -1) {
            buf.write(symBytes, 0, size);
        }

        return new String(buf.toByteArray(), charset);

    }
}
